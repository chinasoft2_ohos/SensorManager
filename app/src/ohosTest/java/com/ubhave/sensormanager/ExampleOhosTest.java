package com.ubhave.sensormanager;

import com.ubhave.sensormanager.utils.IPUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ubhave.sensormanager", actualBundleName);
    }

    @Test
    public void testIpUtil() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        long ipLong = IPUtil.ipToLong("219.239.110.138");
        assertEquals(ipLong,3689901706L);
        String ip = IPUtil.longToIP(18537472);
        assertEquals(ip,"0.220.26.1");
    }


}