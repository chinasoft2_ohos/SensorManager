/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.slice;

import com.ubhave.sensormanager.BuildConfig;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.ability.LocationAbility;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
//        findComponentById(ResourceTable.Id_phone_state_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_sms_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_battery_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_connection_state_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_screen_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_bluetooth_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_connection_strength_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_temperature_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_humidity_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_light_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_pressure_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_magnetic_field_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_accelerometer_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_gyroscope_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_microphone_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_call_content_btn).setClickedListener(this::onClick);
//        findComponentById(ResourceTable.Id_sms_content_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_step_counter_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_wifi_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_proximity_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_passive_location_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_phone_radio_btn).setClickedListener(this::onClick);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_temperature_btn://温度传感器
                Operation temperatureOperation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.TemperatureAbility")
                        .build();
                Intent temperatureIntent = new Intent();
                temperatureIntent.setOperation(temperatureOperation);
                startAbility(temperatureIntent);
                break;

            case ResourceTable.Id_humidity_btn://湿度传感器
                navigateTo(".ability.HumidityAbility");
                break;
            case ResourceTable.Id_light_btn://光传感器
                navigateTo(".ability.LightAbility");
                break;
            case ResourceTable.Id_pressure_btn://压力传感器
                navigateTo(".ability.PressureAbility");
                break;
            case ResourceTable.Id_magnetic_field_btn://磁场传感器
                    navigateTo(".ability.MagneticFieldAbility");
                break;
            case ResourceTable.Id_battery_btn: {
                LogUtil.loge("===TAG");
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.BatteryAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_phone_state_btn: {
                LogUtil.loge("===TAG");
                Operation callOperation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.PhoneStateAbility")
                        .build();
                Intent callIntent = new Intent();
                callIntent.setOperation(callOperation);
                startAbility(callIntent);
                break;
            }
            case ResourceTable.Id_sms_btn: {
                LogUtil.loge("===TAG");
                Operation smsOperation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.SMSAbility")
                        .build();
                Intent smsIntent = new Intent();
                smsIntent.setOperation(smsOperation);
                startAbility(smsIntent);
                break;
            }

            case ResourceTable.Id_connection_state_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.ConnectionStateAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_screen_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.ScreenAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_bluetooth_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.BluetoothAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }

            case ResourceTable.Id_connection_strength_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.ConnectionStrengthAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_accelerometer_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.AccelerometerAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_gyroscope_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.GyroscopeAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_microphone_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.MicrophoneAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_step_counter_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.StepCounterAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_wifi_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.WifiAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_proximity_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.ProximityAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_call_content_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.CallContentReaderAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_passive_location_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(LocationAbility.class)
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_phone_radio_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.PhoneRadioAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }
            case ResourceTable.Id_sms_content_btn: {
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(BuildConfig.PACKAGE_NAME)
                        .withAbilityName(BuildConfig.PACKAGE_NAME + ".ability.SmsContentReaderAbility")
                        .build();
                Intent i = new Intent();
                i.setOperation(operation);
                startAbility(i);
                break;
            }

        }
    }
    public void navigateTo(String name){
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(BuildConfig.PACKAGE_NAME)
                .withAbilityName(BuildConfig.PACKAGE_NAME + name)
                .build();
        Intent i = new Intent();
        i.setOperation(operation);
        startAbility(i);
    }
}
