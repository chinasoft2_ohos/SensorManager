/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pull.AccelerometerData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.bundle.IBundleManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;

import java.text.SimpleDateFormat;
import java.util.*;

public class AccelerometerAbility extends Ability implements SensorDataListener, Component.ClickedListener {

    private int subscriptionId;
    private ESSensorManager sensorManager;

    private Text text_data;
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    private SensorData mSensorData;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_accelerometer);
        text_data = (Text) findComponentById(ResourceTable.Id_accelerometer_data);
        findComponentById(ResourceTable.Id_start_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_stop_btn).setClickedListener(this::onClick);
        requestPermissions();
    }

    private void requestPermissions() {

        if (verifySelfPermission("ohos.permission.ACCELEROMETER") != IBundleManager.PERMISSION_GRANTED) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.ACCELEROMETER")) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(new String[]{"ohos.permission.ACCELEROMETER"}, 1001);
            } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
            }
        } else {
            // 权限已被授予
            System.out.println("已授权");
        }
    }


    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1001: {
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0
                        && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    // 权限被授予
                    // 注意：因时间差导致接口权限检查时有无权限，所以对那些因无权限而抛异常的接口进行异常捕获处理
                    System.out.println("已授权");
                } else {
                    // 权限被拒绝
                }
                break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start_btn:
                LogUtil.loge("===Id_start_btn");
                try {
                    text_data.setText("数据获取中,请耐心等待...");
                    sensorManager = ESSensorManager.getSensorManager(getContext());
                    subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_ACCELEROMETER, this);
                } catch (ESException e) {
                    e.printStackTrace();
                }
                break;
            case ResourceTable.Id_stop_btn:
                LogUtil.loge("===Id_stop_btn");
                if (sensorManager != null) {
                    try {
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public void onDataSensed(SensorData data) {
        mSensorData = data;
        parseData(mSensorData);
        LogUtil.loge("===onDataSensed: " + data.toString());



    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {
        LogUtil.loge("===onCrossingLowBatteryThreshold: " + isBelowThreshold);
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        Optional<Sequenceable> sensorDataOptional = inState.getSequenceable("sensorData");
        if(sensorDataOptional.isPresent()) {
            mSensorData = (SensorData) sensorDataOptional.get();
            parseData(mSensorData);
        }
    }

    private void parseData(SensorData data) {
        if (!(data instanceof AccelerometerData)) {
            return;
        }

        AccelerometerData accelerometerData = (AccelerometerData) data;
        ArrayList<float[]> sensorReadings = accelerometerData.getSensorReadings();
        ArrayList<Long> sensorTimestamps = accelerometerData.getSensorReadingTimestamps();
        int size = Math.min(sensorReadings.size(), 50);
        List<Float> xAxis = new ArrayList<>();
        List<Float> yAxis = new ArrayList<>();
        List<Float> zAxis = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            float[] item = sensorReadings.get(i);
            if (item.length == 3) {
                xAxis.add(item[0]);
                yAxis.add(item[1]);
                zAxis.add(item[2]);
            }

        }
        List<Long> sensorSubTimestamps = sensorTimestamps.subList(0, size);


        StringBuffer buffer = new StringBuffer();
        long timestamp = accelerometerData.getTimestamp();
        buffer.append("当前加速度信息: \n");
        Date date = new Date();
        date.setTime(timestamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateString = dateFormat.format(date);
        buffer.append("senseStartTime: ").append(dateString).append("\n");
        buffer.append("senseStartTimeMillis: ").append(timestamp).append("\n");
        buffer.append("x方向加速度: ").append(xAxis.toString()).append("\n");
        buffer.append("y方向加速度: ").append(yAxis.toString()).append("\n");
        buffer.append("z方向加速度: ").append(zAxis.toString()).append("\n");
        buffer.append("sensor时间戳: ").append(sensorSubTimestamps.toString()).append("\n");

        handler.postTask(() -> text_data.setText(buffer.toString()));
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        if(mSensorData != null) {
            outState.putSequenceableObject("sensorData", mSensorData);
        }
    }
}