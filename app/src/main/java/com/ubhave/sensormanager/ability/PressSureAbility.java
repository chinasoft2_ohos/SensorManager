/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.env.LightData;
import com.ubhave.sensormanager.data.env.PressureData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

public class PressSureAbility extends Ability implements Component.ClickedListener, SensorDataListener {
    private Button startBtn;
    private Button stopBtn;
    private Text temperatureData;
    private ESSensorManager sensorManager;
    private int subscriptionId;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_light);
        startBtn = (Button) findComponentById(ResourceTable.Id_start_btn);
        stopBtn = (Button) findComponentById(ResourceTable.Id_stop_btn);
        temperatureData = (Text) findComponentById(ResourceTable.Id_data_temperature);
        startBtn.setClickedListener(this);
        stopBtn.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        switch (id){
            case ResourceTable.Id_start_btn:
                try {
                    sensorManager = ESSensorManager.getSensorManager(getContext());
                    LogUtil.loge("===走了走了第一步:");
                    subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_PRESSURE, this);
                    LogUtil.loge("===走了走了第二步:"+subscriptionId);
                } catch (ESException e) {
                    ToastDialog toastDialog = new ToastDialog(getContext());
                    toastDialog.setText("你的手机不支持压力传感器");
                    toastDialog.setAlignment(LayoutAlignment.CENTER);
                    toastDialog.show();
                    e.printStackTrace();
                }
                break;
            case ResourceTable.Id_stop_btn:
                if (sensorManager != null) {
                    try {
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {
                        e.printStackTrace();
                    }
                }
                break;

        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDataSensed(SensorData data) {
        if(data instanceof PressureData){
            PressureData pressureData = (PressureData) data;
            float value = pressureData.getValue();
            LogUtil.loge("获取到的空气压力为:"+value+"hPa");
            String text = "获取到的空气压力为："+value+"hPa";
            LogUtil.loge(text);
            getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    temperatureData.setText(text);
                }
            });
        }
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {

    }
}
