/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.env.AmbientTemperatureData;
import com.ubhave.sensormanager.data.env.HumidityData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;

import java.util.Optional;

/**
 * 湿度传感器
 */
public class HumidityAbility extends Ability implements Component.ClickedListener, SensorDataListener {
    private Button startBtn;
    private Button stopBtn;
    private Text temperatureData;
    private ESSensorManager sensorManager;
    private int subscriptionId;
    private SensorData mSensorData;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_humidity);
        startBtn = (Button) findComponentById(ResourceTable.Id_start_btn);
        stopBtn = (Button) findComponentById(ResourceTable.Id_stop_btn);
        temperatureData = (Text) findComponentById(ResourceTable.Id_data_temperature);
        startBtn.setClickedListener(this);
        stopBtn.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        switch (id){
            case ResourceTable.Id_start_btn:
                try {
                    sensorManager = ESSensorManager.getSensorManager(getContext());
                    subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_HUMIDITY, this);
                    LogUtil.loge("===走了走x了"+subscriptionId);
                } catch (ESException e) {
                    e.printStackTrace();
                    ToastDialog toastDialog = new ToastDialog(getContext());
                    toastDialog.setText("你的手机不支持湿度传感器");
                    toastDialog.setAlignment(LayoutAlignment.CENTER);
                    toastDialog.show();
                }
                break;
            case ResourceTable.Id_stop_btn:
                if (sensorManager != null) {
                    try {
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {

                        e.printStackTrace();

                    }
                }
                break;

        }
    }

    @Override
    public void onDataSensed(SensorData data) {
//        LogUtil.loge("是否有回调了??");
        mSensorData = data;
        parseData(mSensorData);

    }

    private void parseData(SensorData data) {
        if(data instanceof HumidityData){
            HumidityData humidityData = (HumidityData) data;
            float value = humidityData.getValue();
            LogUtil.loge("获取到的湿度为:"+value+"%");
            String text = "获取到的湿度为:"+value+"%";
            getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    temperatureData.setText(text);
                }
            });
        }
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        Optional<Sequenceable> sensorDataOptional = inState.getSequenceable("sensorData");
        if(sensorDataOptional.isPresent()) {
            mSensorData = (SensorData) sensorDataOptional.get();
            parseData(mSensorData);
        }
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        if(mSensorData != null) {
            outState.putSequenceableObject("sensorData", mSensorData);
        }
    }
}
