/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.push.SmsData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;

import java.util.Optional;

public class SMSAbility extends Ability implements Component.ClickedListener, SensorDataListener {

    private Text dataSMS;
    private Button startBtn;
    private Button stopBtn;
    private ESSensorManager sensorManager;
    private int subscriptionId;
    private SensorData mSensorData;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_sms);
        dataSMS = (Text) findComponentById(ResourceTable.Id_data_sms);
        startBtn = (Button) findComponentById(ResourceTable.Id_start_btn);
        stopBtn = (Button) findComponentById(ResourceTable.Id_stop_btn);
        startBtn.setClickedListener(this);
        stopBtn.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start_btn:
                LogUtil.loge("===Id_start_btn");
                try {
                    sensorManager = ESSensorManager.getSensorManager(getContext());
                    subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_SMS, this);
                } catch (ESException e) {
                    e.printStackTrace();
                }
                break;
            case ResourceTable.Id_stop_btn:
                LogUtil.loge("===Id_stop_btn");
                if (sensorManager != null) {
                    try {
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtil.loge("===Id_stop_btn");
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDataSensed(SensorData data) {
        LogUtil.loge("===onDataSensed: " + data.toString());
        mSensorData = data;
        parseData(mSensorData);

    }

    private void parseData(SensorData data) {
        if (!(data instanceof SmsData)) {
            return;
        }
        SmsData smsData = (SmsData) data;
        StringBuffer buffer = new StringBuffer();
        buffer.append("SMS信息: \n");
        buffer.append("地址:").append(smsData.getAddress()).append("\n");
        buffer.append("文字数量:").append(smsData.getNoOfWords()).append("\n");
        buffer.append("文本长度:").append(smsData.getContentLength()).append("\n");
        buffer.append("短信类型:").append(smsData.getMessageType()).append("\n");
        dataSMS.setText(buffer.toString());
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {

    }
    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        Optional<Sequenceable> sensorDataOptional = inState.getSequenceable("sensorData");
        if(sensorDataOptional.isPresent()) {
            mSensorData = (SensorData) sensorDataOptional.get();
            parseData(mSensorData);
        }
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        if(mSensorData != null) {
            outState.putSequenceableObject("sensorData", mSensorData);
        }
    }
}
