/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.push.ConnectionStrengthData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class ConnectionStrengthAbility extends Ability implements SensorDataListener, Component.ClickedListener {

    private int subscriptionId;
    private ESSensorManager sensorManager;

    private Text text_data;
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    private SensorData mSensorData;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_connection_strength);
        text_data = (Text) findComponentById(ResourceTable.Id_connection_strength_data);
        findComponentById(ResourceTable.Id_start_btn).setClickedListener(this::onClick);
        findComponentById(ResourceTable.Id_stop_btn).setClickedListener(this::onClick);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start_btn:
                LogUtil.loge("===Id_start_btn");
                try {
                    sensorManager = ESSensorManager.getSensorManager(getContext());
                    subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_CONNECTION_STRENGTH, this);
                } catch (ESException e) {
                    e.printStackTrace();
                }
                break;
            case ResourceTable.Id_stop_btn:
                LogUtil.loge("===Id_stop_btn");
                if (sensorManager != null) {
                    try {
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    public void onDataSensed(SensorData data) {
        LogUtil.loge("===onDataSensed: " + data.toString());
        mSensorData = data;
        parseData(mSensorData);

    }

    private void parseData(SensorData data) {
        if(!(data instanceof ConnectionStrengthData)) {
            return;
        }
        ConnectionStrengthData strengthData = (ConnectionStrengthData) data;
        StringBuffer buffer = new StringBuffer();
        int signalLevel = strengthData.getSignalLevel();
        long timestamp = strengthData.getTimestamp();
//        SensorConfig sensorConfig = strengthData.getSensorConfig();
        buffer.append("当前连接强度信息: \n");
        Date date = new Date();
        date.setTime(timestamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateString = dateFormat.format(date);
        buffer.append("senseStartTime: ").append(dateString).append("\n");
        buffer.append("senseStartTimeMillis: ").append(timestamp).append("\n");
        buffer.append("信号电平(singalLevel): ").append(signalLevel).append("\n");

        handler.postTask(() -> text_data.setText(buffer.toString()));
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {
        LogUtil.loge("===onCrossingLowBatteryThreshold: " + isBelowThreshold);
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        Optional<Sequenceable> sensorDataOptional = inState.getSequenceable("sensorData");
        if(sensorDataOptional.isPresent()) {
            mSensorData = (SensorData) sensorDataOptional.get();
            parseData(mSensorData);
        }
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        if(mSensorData != null) {
            outState.putSequenceableObject("sensorData", mSensorData);
        }
    }
}