/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.ability;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pull.BluetoothData;
import com.ubhave.sensormanager.data.pull.ESBluetoothDevice;
import com.ubhave.sensormanager.provider.BluetoothItemProvider;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bluetooth.BluetoothHost;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class BluetoothAbility extends Ability implements SensorDataListener, Component.ClickedListener {

    private ListContainer listContainer;
    private Button startBtn;
    private Button stopBtn;
    private Text tip;
    private ESSensorManager sensorManager;
    private int subscriptionId;
    private List<ESBluetoothDevice> list = new ArrayList<>();
    private BluetoothItemProvider bluetoothItemProvider;
    private SensorData mSensorData;
    private BluetoothHost bluetoothHost;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        requestPermissionsFromUser(new String[]{"ohos.permission.LOCATION"}, 0);
        setUIContent(ResourceTable.Layout_ability_bluetooth);
        tip = (Text) findComponentById(ResourceTable.Id_tip);
        startBtn = (Button) findComponentById(ResourceTable.Id_start_btn);
        stopBtn = (Button) findComponentById(ResourceTable.Id_stop_btn);
        startBtn.setClickedListener(this);
        stopBtn.setClickedListener(this);
        bluetoothHost = BluetoothHost.getDefaultHost(getContext());

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_data_bluetooth);
        bluetoothItemProvider = new BluetoothItemProvider(list, this);
        listContainer.setItemProvider(bluetoothItemProvider);
        listContainer.setItemClickedListener((listContainer, component, position, id) -> {
            ESBluetoothDevice item = (ESBluetoothDevice) listContainer.getItemProvider().getItem(position);
            new ToastDialog(getContext())
                    .setText("clicked:" + item.getBluetoothDeviceAddress())
                    .setAlignment(LayoutAlignment.CENTER)
                    .show();
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (sensorManager != null) {
            try {
                sensorManager.unsubscribeFromSensorData(subscriptionId);
            } catch (ESException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDataSensed(SensorData data) {
        mSensorData = data;
        parseData(mSensorData);

    }

    private void parseData(SensorData data) {
        if (!(data instanceof BluetoothData)) {
            getContext().getUITaskDispatcher().asyncDispatch(() -> {
                tip.setText("数据异常!");
            });
            return;
        }
        BluetoothData bluetoothData = (BluetoothData) data;
        showBtList(bluetoothData);
    }

    private void showBtList(BluetoothData bluetoothData) {
        getContext().getUITaskDispatcher().delayDispatch(() -> {
            StringBuffer buffer = new StringBuffer();
            buffer.append("数据").append("\n");
            long timestamp = bluetoothData.getTimestamp();
            Date date = new Date();
            date.setTime(timestamp);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String dateString = dateFormat.format(date);

            buffer.append("senseStartTime: ").append(dateString).append("\n");
            buffer.append("senseStartTimeMillis: ").append(timestamp);

            tip.setText(buffer.toString());
            if (null != bluetoothData) {
                final ArrayList<ESBluetoothDevice> bluetoothDevices = bluetoothData.getBluetoothDevices();
                if (null != bluetoothDevices && bluetoothDevices.size() > 0) {
                    list.clear();
                    list.addAll(bluetoothDevices);
                    listContainer.setVisibility(Component.VISIBLE);
                    bluetoothItemProvider.notifyDataChanged();

                } else {
                    tip.setText("暂无数据!");
                }
            } else {
                tip.setText("暂无数据!");
            }
        }, 1);
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start_btn:
                try {
                    if (btIsOn()) {
                        tip.setText("蓝牙搜索中,请勿继续操作...");
                        sensorManager = ESSensorManager.getSensorManager(getContext());
                        subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_BLUETOOTH, this);
                        System.out.println(subscriptionId);
                    } else {
                        tip.setText("请先打开蓝牙,然后点击开始");
                        bluetoothHost.enableBt();
                        return;
                    }

                } catch (ESException e) {
                    e.printStackTrace();
                }
                break;
            case ResourceTable.Id_stop_btn:
                if (sensorManager != null) {
                    try {
                        tip.setText("结束搜索!");
                        sensorManager.unsubscribeFromSensorData(subscriptionId);
                    } catch (ESException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private boolean btIsOn() {
        if (null!=bluetoothHost){
            if (bluetoothHost.getBtState()==BluetoothHost.STATE_OFF){
                return false;
            }else {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        Optional<Sequenceable> sensorDataOptional = inState.getSequenceable("sensorData");
        if (sensorDataOptional.isPresent()) {
            mSensorData = (SensorData) sensorDataOptional.get();
            parseData(mSensorData);
        }
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        if (mSensorData != null) {
            outState.putSequenceableObject("sensorData", mSensorData);
        }
    }
}
