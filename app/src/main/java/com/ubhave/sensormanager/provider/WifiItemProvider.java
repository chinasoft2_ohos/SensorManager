/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.provider;

import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.data.pull.ESBluetoothDevice;
import com.ubhave.sensormanager.data.pull.WifiScanResult;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

import java.util.List;

/**
 * wifi适配器
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/25
 */
public class WifiItemProvider extends RecycleItemProvider {
    private List<WifiScanResult> list;
    private Ability ability;

    public WifiItemProvider(List<WifiScanResult> list, Ability ability) {
        this.list = list;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt = convertComponent;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_item_wifi, null, false);
        }
        WifiScanResult sampleItem = list.get(position);
        Text ssid = (Text) cpt.findComponentById(ResourceTable.Id_item_ssid);
        ssid.setText("ssid: " + sampleItem.getSsid());
        Text bssid = (Text) cpt.findComponentById(ResourceTable.Id_item_bssid);
        bssid.setText("bssid: " + sampleItem.getBssid());
        Text level = (Text) cpt.findComponentById(ResourceTable.Id_item_level);
        level.setText("level: " + sampleItem.getLevel());
        Text capabilities = (Text) cpt.findComponentById(ResourceTable.Id_item_capabilities);
        capabilities.setText("capabilities: " + sampleItem.getCapabilities());
        Text frequency = (Text) cpt.findComponentById(ResourceTable.Id_item_frequency);
        frequency.setText("frequency: " + sampleItem.getFrequency());
        return cpt;
    }
}
