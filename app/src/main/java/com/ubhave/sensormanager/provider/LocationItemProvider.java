/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.provider;

import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.bean.LocationBean;
import com.ubhave.sensormanager.data.pull.WifiScanResult;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.location.Location;
import ohos.utils.Sequenceable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 位置信息适配器
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/25
 */
public class LocationItemProvider extends RecycleItemProvider {
    private List<Sequenceable> list;
    private Ability ability;

    public LocationItemProvider(List<Sequenceable> list, Ability ability) {
        this.list = list;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt = convertComponent;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_item_location, null, false);
        }
        LocationBean sampleItem = (LocationBean) list.get(position);
        Text latitude = (Text) cpt.findComponentById(ResourceTable.Id_item_latitude);
        latitude.setText("latitude: " + sampleItem.getLatitude());
        Text longitude = (Text) cpt.findComponentById(ResourceTable.Id_item_longitude);
        longitude.setText("longitude: " + sampleItem.getLongitude());
        Text accuracy = (Text) cpt.findComponentById(ResourceTable.Id_item_accuracy);
        accuracy.setText("accuracy: " + sampleItem.getAccuracy());
        Text direction = (Text) cpt.findComponentById(ResourceTable.Id_item_direction);
        direction.setText("direction: " + sampleItem.getDirection());
        Text speed = (Text) cpt.findComponentById(ResourceTable.Id_item_speed);
        speed.setText("speed: " + sampleItem.getSpeed());
        Long timeMillis = sampleItem.getTimeMillis();
        Text time = (Text) cpt.findComponentById(ResourceTable.Id_item_time);
        time.setText("time: " + timeMillis);

        Date date = new Date();
        date.setTime(timeMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateString = dateFormat.format(date);

        Text localTime = (Text) cpt.findComponentById(ResourceTable.Id_item_local_time);
        localTime.setText("local_time: " + dateString);
        return cpt;
    }
}
