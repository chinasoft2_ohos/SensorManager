/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.provider;

import com.ubhave.sensormanager.ResourceTable;
import com.ubhave.sensormanager.data.pull.ESBluetoothDevice;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.Text;

import java.util.List;

/**
 * 蓝牙适配器
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/25
 */
public class BluetoothItemProvider extends RecycleItemProvider {
    private List<ESBluetoothDevice> list;
    private Ability ability;

    public BluetoothItemProvider(List<ESBluetoothDevice> list, Ability ability) {
        this.list = list;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Component cpt = convertComponent;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_item_bluetooth, null, false);
        }
        ESBluetoothDevice sampleItem = list.get(position);
        Text address = (Text) cpt.findComponentById(ResourceTable.Id_item_address);
        address.setText("address: " + sampleItem.getBluetoothDeviceAddress());
        Text name = (Text) cpt.findComponentById(ResourceTable.Id_item_name);
        name.setText("name: " + sampleItem.getBluetoothDeviceName());
        Text timestamp = (Text) cpt.findComponentById(ResourceTable.Id_item_timestamp);
        timestamp.setText("timestamp: " + sampleItem.getTimestamp());
        return cpt;
    }
}
