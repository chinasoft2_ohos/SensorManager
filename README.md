# SensorManager

#### 项目介绍
- 项目名称：SensorManager
- 所属系列：openharmony的第三方组件适配移植
- 功能：这个一个关于传感器相关的功能用法，里面包含多种传感器的使用方法以及测试Demo
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：master 分支

#### 效果演示
![效果演示](https://images.gitee.com/uploads/images/2021/0609/113548_dd405196_8908462.gif "sensorManager.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:sensorManager:1.0.0')
    ......  
 }
 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

##### 1.进行相关权限的授权
##### 2.相关调用
###### (1).订阅
```
 try {
    ESSensorManager sensorManager = ESSensorManager.getSensorManager(getContext());
    int subscriptionId = sensorManager.subscribeToSensorData(SensorUtils.SENSOR_TYPE_ACCELEROMETER, this);
 } catch (ESException e) {
    e.printStackTrace();
 }
```

###### (2).释放订阅
```
 try {
    sensorManager.unsubscribeFromSensorData(subscriptionId);
 } catch (ESException e) {
    e.printStackTrace();
 }
```

##### 3.相关type参数
```
    public final static int SENSOR_TYPE_ACCELEROMETER = 5001;
    public final static int SENSOR_TYPE_BATTERY = 5002;
    public final static int SENSOR_TYPE_BLUETOOTH = 5003;
    public final static int SENSOR_TYPE_LOCATION = 5004;
    public final static int SENSOR_TYPE_MICROPHONE = 5005;
    public final static int SENSOR_TYPE_PHONE_STATE = 5006;
    public final static int SENSOR_TYPE_PROXIMITY = 5007;
    public final static int SENSOR_TYPE_SCREEN = 5008;
    public final static int SENSOR_TYPE_SMS = 5009;
    public final static int SENSOR_TYPE_WIFI = 5010;
    public final static int SENSOR_TYPE_CONNECTION_STATE = 5011;
    public final static int SENSOR_TYPE_SMS_CONTENT_READER = 5013;
    public final static int SENSOR_TYPE_CALL_CONTENT_READER = 5014;
    public final static int SENSOR_TYPE_GYROSCOPE = 5016;
    public final static int SENSOR_TYPE_LIGHT = 5017;
    public final static int SENSOR_TYPE_PHONE_RADIO = 5018;
    public final static int SENSOR_TYPE_CONNECTION_STRENGTH = 5019;
    public final static int SENSOR_TYPE_PASSIVE_LOCATION = 5020;
    public final static int SENSOR_TYPE_AMBIENT_TEMPERATURE = 5021;
    public final static int SENSOR_TYPE_PRESSURE = 5022;
    public final static int SENSOR_TYPE_HUMIDITY = 5023;
    public final static int SENSOR_TYPE_MAGNETIC_FIELD = 5024;
    public final static int SENSOR_TYPE_STEP_COUNTER = 5025;
    public final static int SENSOR_TYPE_INTERACTION = 5026;
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 待实现

* 温度传感器
* 湿度传感器
* 呼叫内容传感器
* 短信内容传感器
* 麦克风传感器
* 电话状态传感器
* SMS传感器


#### 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息

Copyright (c), University of Cambridge 

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
