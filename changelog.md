## 1.0.0
发布1.0.0 release版本

## 0.0.1-SNAPSHOT
SensorManager组件openharmony第一个beta版本

#### api差异

* 1.无线电传感器只能拿到国家区域代码(lac)，SIM卡的cid API调用为空
* 2.连接强度传感器无法获取到强度strength相关的数据，只有信号电平(SignalLevel)数据

#### 已实现功能

* 该库基本功能已全部实现

#### 未实现功能

* 温度传感器
* 湿度传感器
* 呼叫内容传感器
* 短信内容传感器
* 麦克风传感器
* 电话状态传感器
* SMS传感器

