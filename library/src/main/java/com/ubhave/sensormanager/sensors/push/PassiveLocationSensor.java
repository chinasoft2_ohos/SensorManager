/* **************************************************
 Copyright (c) 2014, Idiap
Hugues Salamin, hugues.salamin@idiap.ch

This file was developed to add passive location sensor to the SensorManager library
from https://github.com/nlathia/SensorManager.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.push;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.config.GlobalConfig;
import com.ubhave.sensormanager.config.push.PassiveLocationConfig;
import com.ubhave.sensormanager.data.push.PassiveLocationData;
import com.ubhave.sensormanager.process.push.PassiveLocationProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.location.Location;
import ohos.location.Locator;
import ohos.location.LocatorCallback;
import ohos.location.RequestParam;

public class PassiveLocationSensor extends AbstractPushSensor {
    private static final String TAG = "PassiveLocationSensor";
    private volatile static PassiveLocationSensor passiveLocationSensor;
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            "ohos.permission.LOCATION"
    };

    private LocatorCallback locationListener;
    private Locator locationManager;

    public static PassiveLocationSensor getSensor(final Context context) throws ESException {
        /*
         * Implement a double checked lock, using volatile. The result variable
         * is for speed reason (avoid reading the volatile member too many time
         */
        PassiveLocationSensor result = passiveLocationSensor;
        if (result == null) {
            synchronized (PassiveLocationSensor.class) {
                result = passiveLocationSensor;
                if (result == null) {
                    if (allPermissionsGranted(context, REQUIRED_PERMISSIONS)) {
                        passiveLocationSensor = result = new PassiveLocationSensor(context);
                    } else {

                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_CONNECTION_STRENGTH);
                    }
                }
            }
        }
        return result;
    }

    private PassiveLocationSensor(final Context context) {
        super(context);
        locationListener = new LocatorCallback() {
            @Override
            public void onLocationReport(Location location) {
                try {
                    PassiveLocationProcessor processor = (PassiveLocationProcessor) getProcessor();
                    PassiveLocationData locationtData = processor.process(System.currentTimeMillis(), location, sensorConfig.clone());
                    onDataSensed(locationtData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStatusChanged(int i) {

            }

            @Override
            public void onErrorReport(int i) {

            }
        };
    }

    @Override
    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_PASSIVE_LOCATION;
    }

    @Override
    protected void onBroadcastReceived(final Context context, final Intent intent) {
        // We are not listening to broadcast so this is empty
    }

    @Override
    protected MatchingSkills getIntentFilters() {
        return null;
    }

    @Override
    protected boolean startSensing() {
        locationManager = new Locator(applicationContext);
        try {
            //Passive
            final Long time = (Long) getSensorConfig(PassiveLocationConfig.MIN_TIME);
            final Float distance = (Float) getSensorConfig(PassiveLocationConfig.MIN_DISTANCE);
            RequestParam requestParam = new RequestParam(RequestParam.PRIORITY_UNSET, time.intValue(), distance.intValue());
            locationManager.startLocating(requestParam, locationListener);
        } catch (ESException e) {
            if (GlobalConfig.shouldLog()) {
                LogUtil.loge(TAG, "Error getting parameter value for sensor");
            }
            e.printStackTrace();
        }
        return true;
    }

    @Override
    protected void stopSensing() {
        if (locationManager != null) {
            locationManager.stopLocating(locationListener);
        }
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }

}
