/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.push;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.config.GlobalConfig;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.sensors.AbstractSensor;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.*;
import ohos.rpc.RemoteException;

public abstract class AbstractPushSensor extends AbstractSensor implements PushSensor {
    protected SensorDataListener sensorDataListener;
    protected CommonEventSubscriber broadcastReceiver;

    public AbstractPushSensor(Context context) {
        super(context);
        MatchingSkills skills = getIntentFilters();
        CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(skills);
        broadcastReceiver = new CommonEventSubscriber(subscribeInfo) {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                if (isSensing) {
                    onBroadcastReceived(context, commonEventData.getIntent());
                } else if (GlobalConfig.shouldLog()) {
                    LogUtil.loge(getLogTag(), "BroadcastReceiver.onReceive() called while not sensing.");
                }
            }
        };
    }

    protected abstract void onBroadcastReceived(Context context, Intent intent);

    protected abstract MatchingSkills getIntentFilters();

    public void startSensing(SensorDataListener listener) throws ESException {
        if (isSensing) {
            // sensing already started
            if (GlobalConfig.shouldLog()) {
                LogUtil.loge(getLogTag(), "sensing already sensing");
            }
            throw new ESException(ESException.SENSOR_ALREADY_SENSING, "sensor already sensing");
        }

        this.sensorDataListener = listener;
        startSensing();
        // register broadcast receiver
        try {
            CommonEventManager.subscribeCommonEvent(broadcastReceiver);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new ESException(ESException.UNKNOWN_SENSOR_TYPE, "broadcastReceiver subscribe invalid");
        }
        isSensing = true;
        if (GlobalConfig.shouldLog()) {
            LogUtil.loge(getLogTag(), "Sensing started.");
        }
    }

    public void stopSensing(SensorDataListener listener) throws ESException {
        if (!isSensing) {
            // sensing already started
            if (GlobalConfig.shouldLog()) {
                LogUtil.loge(getLogTag(), "sensor not sensing");
            }
            throw new ESException(ESException.SENSOR_NOT_SENSING, "sensor not sensing");
        }

        stopSensing();
        try {
            CommonEventManager.unsubscribeCommonEvent(broadcastReceiver);
        } catch (IllegalArgumentException | RemoteException e) {
            // Receiver not registered
        }

        isSensing = false;
        if (GlobalConfig.shouldLog()) {
            LogUtil.loge(getLogTag(), "Sensing stopped.");
        }
    }

    protected void onDataSensed(final SensorData sensorData) {
        if (sensorDataListener != null) {
            sensorDataListener.onDataSensed(sensorData);
        }
    }
}
