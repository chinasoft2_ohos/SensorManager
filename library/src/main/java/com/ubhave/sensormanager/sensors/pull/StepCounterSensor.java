/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.pull;

import java.util.Calendar;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.config.pull.MotionSensorConfig;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pull.StepCounterData;
import com.ubhave.sensormanager.process.pull.StepCounterProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.app.Context;
import ohos.miscservices.timeutility.Time;
import ohos.sensor.agent.CategoryMotionAgent;
import ohos.sensor.agent.SensorAgent;
import ohos.sensor.bean.*;
import ohos.sensor.data.CategoryMotionData;
import ohos.sensor.listener.ICategoryMotionDataCallback;

/**
 * 计步器
 */
public class StepCounterSensor extends AbstractPullSensor {
    protected final static Object lock = new Object();
    private static final String TAG = "StepCounterSensor";
    private volatile static StepCounterSensor stepCounterSensor;
    private final ICategoryMotionDataCallback listener; // data listener
    private final SensorAgent sensorManager; // Controls the hardware sensor

    private StepCounterData data;
    private float numSteps;
    private long lastBoot;

    public static StepCounterSensor getSensor(final Context context) throws ESException {
        StepCounterSensor localStepCounterSensor = stepCounterSensor;
        if (localStepCounterSensor == null) {
            synchronized (lock) {
                localStepCounterSensor = stepCounterSensor;
                if (localStepCounterSensor == null) {
                    if (permissionGranted(context, "ohos.permission.ACTIVITY_MOTION")) {
                        stepCounterSensor = localStepCounterSensor = new StepCounterSensor(context);
                    } else {
                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_STEP_COUNTER);
                    }
                }
            }
        }
        return stepCounterSensor;
    }

    protected StepCounterSensor(final Context context) throws ESException {
        super(context);
        sensorManager = new CategoryMotionAgent();
        if (getSensor() == null) {
            throw new ESException(ESException.SENSOR_UNAVAILABLE, getLogTag() + " is null (e.g., missing from device).");
        } else {
            listener = getListener();
        }
    }

    protected SensorBase getSensor() {

        return sensorManager.getSingleSensor(CategoryMotion.SENSOR_TYPE_PEDOMETER);
    }

    protected ICategoryMotionDataCallback getListener() {
        return new ICategoryMotionDataCallback() {
            @Override
            public void onSensorDataModified(CategoryMotionData categoryMotionData) {
                try {
                    if (isSensing) {
                        synchronized (lock) {
                            if (isSensing) {
                                numSteps = categoryMotionData.values[0];
                                long millisSinceSystemBoot = Time.getRealTime();
                                lastBoot = System.currentTimeMillis() - millisSinceSystemBoot;

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTimeInMillis(lastBoot);
                                LogUtil.loge(TAG, "Num steps: " + numSteps);
                                LogUtil.loge(TAG, "Last boot: " + calendar.getTime().toString());
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAccuracyDataModified(CategoryMotion categoryMotion, int i) {

            }

            @Override
            public void onCommandCompleted(CategoryMotion categoryMotion) {

            }
        };
    }

    @Override
    protected boolean startSensing() {
        int sensorDelay = (Integer) sensorConfig.getParameter(MotionSensorConfig.SAMPLING_DELAY);
        boolean registrationSuccess = sensorManager.setSensorDataCallback(listener, getSensor(), sensorDelay);
        return registrationSuccess;
    }

    @Override
    protected void stopSensing() {
        sensorManager.releaseSensorDataCallback(listener);
    }

    @Override
    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_STEP_COUNTER;
    }

    @Override
    protected void processSensorData() {
        LogUtil.loge(TAG, "processSensorData()");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(lastBoot);
        LogUtil.loge(TAG, "Num steps: " + numSteps);
        LogUtil.loge(TAG, "Last boot: " + calendar.getTime().toString());
        StepCounterProcessor processor = (StepCounterProcessor) getProcessor();
        data = processor.process(pullSenseStartTimestamp, numSteps, lastBoot, sensorConfig.clone());
    }

    @Override
    protected String getLogTag() {
        return TAG;
    }

    @Override
    protected SensorData getMostRecentRawData() {
        LogUtil.loge(TAG, "getMostRecentRawData()");
        return data;
    }
}
