/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.pull;

import com.ubhave.sensormanager.utils.LogUtil;
import ohos.app.Context;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public abstract class AbstractMediaSensor extends AbstractPullSensor {
    protected AbstractMediaSensor(Context context) {
        super(context);
    }

    protected abstract String getFileDirectory();

    protected abstract String getFilePrefix();

    protected abstract String getFileSuffix();

    private String getFileName(boolean isUnique) {
        String fileName = getFilePrefix();
        if (isUnique) {
            fileName += "_" + System.currentTimeMillis();
        }
        fileName += getFileSuffix();
        return fileName;
    }

    protected File getMediaFile() {
        String path = getFileDirectory();
        if (path != null) {
            File directory = new File(path);
            if (!directory.exists()) {
                boolean mkdirs = directory.mkdirs();
                if (!mkdirs) {
                    LogUtil.loge("SensorManager", "cannot create file ");
                }
            }
            File file = new File(directory, getFileName(true));
            try {
                LogUtil.loge("SensorManager", "Creating file: " + file.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return file;
        } else {
            File file = new File(applicationContext.getFilesDir(), getFileName(false));
            if (file.exists()) {
                try {
                    Files.delete(file.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return file;
        }
    }
}
