/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.env;


import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.process.env.AmbientTemperatureProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.app.Context;
import ohos.sensor.bean.CategoryEnvironment;
import ohos.sensor.bean.SensorBase;

public class AmbientTemperatureSensor extends AbstractEnvironmentSensor {
    private static final String TAG = "TemperatureSensor";
    private volatile static AmbientTemperatureSensor temperatureSensor;
    protected static final Object lock = new Object();

    public static AmbientTemperatureSensor getSensor(final Context context) throws ESException {
        AmbientTemperatureSensor localAmbientTemperatureSensor = temperatureSensor;
        if (localAmbientTemperatureSensor == null) {
            synchronized (lock) {
                localAmbientTemperatureSensor = temperatureSensor;
                if (localAmbientTemperatureSensor == null) {
                    temperatureSensor = localAmbientTemperatureSensor = new AmbientTemperatureSensor(context);
                }
            }
        }
        return temperatureSensor;
    }

    private AmbientTemperatureSensor(final Context context) throws ESException {
        super(context);
    }

    public String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_AMBIENT_TEMPERATURE;
    }

    @Override
    protected SensorBase getSensor() {
        return sensorManager.getSingleSensor(CategoryEnvironment.SENSOR_TYPE_AMBIENT_TEMPERATURE);
    }

    @Override
    protected SensorData processEvent(ohos.sensor.data.SensorData event) {

        AmbientTemperatureProcessor processor = (AmbientTemperatureProcessor) getProcessor();
        return processor.process(System.currentTimeMillis(), sensorConfig.clone(), event.values[0]);
    }
}
