/* **************************************************
 Copyright (c) 2014, Idiap
 Olivier Bornet, olivier.bornet@idiap.ch

This file was developed to add phone radio sensor to the SensorManager library
from https://github.com/nlathia/SensorManager.

The SensorManager library was developed as part of the EPSRC Ubhave (Ubiquitous
and Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.pull;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.pull.PhoneRadioData;
import com.ubhave.sensormanager.data.pull.PhoneRadioDataList;
import com.ubhave.sensormanager.process.pull.PhoneRadioProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.app.Context;
import ohos.telephony.*;

import java.util.ArrayList;
import java.util.List;

public class PhoneRadioSensor extends AbstractPullSensor {
    private static final String TAG = "PhoneRadioSensor";
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            "ohos.permission.GET_NETWORK_INFO",
    };

    private volatile static PhoneRadioSensor phoneRadioSensor;
    private static Object lock = new Object();

    private ArrayList<PhoneRadioData> visibleCells;
    private volatile PhoneRadioDataList phoneRadioDataList;
    private RadioInfoManager manager;

    public static PhoneRadioSensor getPhoneRadioSensor(final Context context) throws ESException {
        PhoneRadioSensor localPhoneRadioSensor = phoneRadioSensor;
        if (localPhoneRadioSensor == null) {
            synchronized (lock) {
                localPhoneRadioSensor = phoneRadioSensor;
                if (localPhoneRadioSensor == null) {
                    if (allPermissionsGranted(context, REQUIRED_PERMISSIONS)) {
                        phoneRadioSensor = localPhoneRadioSensor = new PhoneRadioSensor(context);
                    } else {
                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_PHONE_RADIO);
                    }
                }
            }
        }
        return phoneRadioSensor;
    }

    private PhoneRadioSensor(Context context) {
        super(context);
    }

    protected String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_PHONE_RADIO;
    }

    protected PhoneRadioDataList getMostRecentRawData() {
        return phoneRadioDataList;
    }

    protected void processSensorData() {
        PhoneRadioProcessor processor = (PhoneRadioProcessor) getProcessor();
        phoneRadioDataList = processor.process(pullSenseStartTimestamp, visibleCells, sensorConfig.clone());
    }

    protected boolean startSensing() {
        new Thread() {
            public void run() {
                try {
                    visibleCells = new ArrayList<PhoneRadioData>();
                    manager = RadioInfoManager.getInstance(applicationContext);

//                    SimInfoManager simInfoManager = SimInfoManager.getInstance(applicationContext);
                    int primarySlotId = manager.getPrimarySlotId();
                    List<SignalInformation> signalInformations = manager.getSignalInfoList(primarySlotId);
//                    String s = manager.getOperatorName(primarySlotId);
//                    int t = manager.getPsRadioTech(primarySlotId);
//                    String imei = manager.getImei(primarySlotId);
//                    int cs = manager.getCsRadioTech(primarySlotId);
                    String code = manager.getIsoCountryCodeForNetwork(primarySlotId);
                    String plm = manager.getPlmnNumeric(primarySlotId);
//                    int nsm = manager.getNetworkSelectionMode(primarySlotId);
//                    String meid = manager.getMeid(primarySlotId);

//                    NetworkState state = manager.getNetworkState(primarySlotId);
//                    String a = state.getLongOperatorName();
//                    String b = state.getShortOperatorName();
//                    String c = state.getPlmnNumeric();

//                    String isoCode = simInfoManager.getSimIccId(simInfoManager.getDefaultVoiceSlotId());
//                    String telephoneNumber = simInfoManager.getSimTelephoneNumber(simInfoManager.getDefaultVoiceSlotId());

                    for (int i = 0; i < signalInformations.size(); i++) {
                        SignalInformation signalInformation = signalInformations.get(i);
                        LogUtil.loge("===signalInformation: " + signalInformation.getNetworkType() + "      " + signalInformation.getSignalLevel());
                    }

                    visibleCells.add(new PhoneRadioData(plm.substring(0, 3), plm.substring(3), code));

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    notifySenseCyclesComplete();
                }
            }
        }.start();

        return true;
    }

    // Called when a scan is finished
    protected void stopSensing() {
    }

}
