/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.push;


import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.config.GlobalConfig;
import com.ubhave.sensormanager.data.push.ConnectionStateData;
import com.ubhave.sensormanager.process.push.ConnectionStateProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import com.ubhave.sensormanager.utils.LogUtil;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.net.NetHandle;
import ohos.net.NetManager;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;

public class ConnectionStateSensor extends AbstractPushSensor {
    private static final String TAG = "ConnectionStateSensor";
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
//            Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE
    };

    private volatile static ConnectionStateSensor connectionSensor;
    private static final Object lock = new Object();

    public static ConnectionStateSensor getSensor(final Context context) throws ESException {
        ConnectionStateSensor localConnectionStateSensor = connectionSensor;
        if (localConnectionStateSensor == null) {
            synchronized (lock) {
                localConnectionStateSensor = connectionSensor;
                if (localConnectionStateSensor == null) {
                    //暂时不需要权限
//                    if (allPermissionsGranted(context, REQUIRED_PERMISSIONS)) {
                    connectionSensor = localConnectionStateSensor = new ConnectionStateSensor(context);
//                    } else {
//                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_CONNECTION_STATE);
//                    }
                }
            }
        }
        return connectionSensor;
    }

    private ConnectionStateSensor(final Context context) {
        super(context);
    }

    @Override
    protected void onBroadcastReceived(Context context, Intent intent) {
        if (isSensing) {
            try {
                NetManager netManager = NetManager.getInstance(context);
                NetHandle activeNetwork = netManager.getDefaultNet();
//                IntentParams params = intent.getParams();
//                int networkType = (int) params.getParam("networkType");
//                NetHandle appNet = netManager.getAppNet();
                WifiDevice wifiMgr = WifiDevice.getInstance(context);
                WifiLinkedInfo wifiInfo = wifiMgr.getLinkedInfo().get();
                ConnectionStateProcessor processor = (ConnectionStateProcessor) getProcessor();
                ConnectionStateData data = processor.process(System.currentTimeMillis(), sensorConfig.clone(), activeNetwork, wifiInfo);
                onDataSensed(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (GlobalConfig.shouldLog()) {
            LogUtil.loge(getLogTag(), "logOnDataSensed() called while not sensing.");
        }
    }

    protected MatchingSkills getIntentFilters() {
        MatchingSkills skills = new MatchingSkills();
        skills.addEvent("android.net.conn.CONNECTIVITY_CHANGE");
        return skills;
    }

    protected String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_CONNECTION_STATE;
    }

    protected boolean startSensing() {
        return true;
    }

    protected void stopSensing() {
        // nothing to do
    }

}
