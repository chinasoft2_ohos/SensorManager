/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.push;

import com.ubhave.sensormanager.data.push.ProximityData;
import com.ubhave.sensormanager.process.push.ProximityProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.sensor.agent.CategoryLightAgent;
import ohos.sensor.agent.SensorAgent;
import ohos.sensor.bean.CategoryLight;
import ohos.sensor.data.CategoryLightData;
import ohos.sensor.listener.ICategoryLightDataCallback;

public class ProximitySensor extends AbstractPushSensor {
    private static final String TAG = "ProximitySensor";

    private volatile static ProximitySensor ProximitySensor;
    private static Object lock = new Object();

    private ICategoryLightDataCallback sensorEventListener;
    private CategoryLightAgent sensorManager;

    public static ProximitySensor getSensor(final Context context) {
        ProximitySensor localProximitySensor = ProximitySensor;
        if (localProximitySensor == null) {
            synchronized (lock) {
                localProximitySensor = ProximitySensor;
                if (localProximitySensor == null) {
                    ProximitySensor = localProximitySensor = new ProximitySensor(context);
                }
            }
        }
        return ProximitySensor;
    }

    private ProximitySensor(Context context) {
        super(context);
        sensorEventListener = new ICategoryLightDataCallback() {
            @Override
            public void onSensorDataModified(CategoryLightData categoryLightData) {
                try {
                    float distance = categoryLightData.values[0];
                    float maxRange = categoryLightData.sensor.getMaxInterval();

                    ProximityProcessor processor = (ProximityProcessor) getProcessor();
                    ProximityData proximityData = processor.process(System.currentTimeMillis(), sensorConfig.clone(),
                            distance, maxRange);
                    onDataSensed(proximityData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAccuracyDataModified(CategoryLight categoryLight, int i) {

            }

            @Override
            public void onCommandCompleted(CategoryLight categoryLight) {

            }
        };
    }

    public String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_PROXIMITY;
    }

    protected void onBroadcastReceived(Context context, Intent intent) {
        // ignore
    }

    protected MatchingSkills getIntentFilters() {
        // no intent filters
        return null;
    }

    protected boolean startSensing() {
//        sensorManager = new CategoryEnvironmentAgent();
        sensorManager = new CategoryLightAgent();
        CategoryLight data = sensorManager.getSingleSensor(CategoryLight.SENSOR_TYPE_PROXIMITY);

        boolean registered = sensorManager.setSensorDataCallback(sensorEventListener, data, SensorAgent.SENSOR_SAMPLING_RATE_NORMAL);
        return registered;
    }

    protected void stopSensing() {
        if (sensorManager != null) {
            sensorManager.releaseSensorDataCallback(sensorEventListener);
        }
    }

}
