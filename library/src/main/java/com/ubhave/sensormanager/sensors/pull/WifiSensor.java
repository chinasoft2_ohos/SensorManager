/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.pull;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.config.pull.PullSensorConfig;
import com.ubhave.sensormanager.data.pull.WifiData;
import com.ubhave.sensormanager.data.pull.WifiScanResult;
import com.ubhave.sensormanager.process.pull.WifiProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.app.Context;
import ohos.event.commonevent.*;
import ohos.rpc.RemoteException;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiEvents;
import ohos.wifi.WifiScanInfo;

import java.util.ArrayList;
import java.util.List;

public class WifiSensor extends AbstractPullSensor {
    private static final String TAG = "WifiSensor";
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            "ohos.permission.GET_WIFI_INFO", "ohos.permission.SET_WIFI_INFO", "ohos.permission.LOCATION"
    };

    private volatile static WifiSensor wifiSensor;
    private static Object lock = new Object();

    private WifiDevice wifiManager;
    private CommonEventSubscriber wifiReceiver;
    private ArrayList<WifiScanResult> wifiScanResults;
    private int cyclesRemaining;
    private WifiData wifiData;

    public static WifiSensor getSensor(final Context context) throws ESException {
        WifiSensor localWifiSensor = wifiSensor;
        if (localWifiSensor == null) {
            synchronized (lock) {
                localWifiSensor = wifiSensor;
                if (localWifiSensor == null) {
                    if (allPermissionsGranted(context, REQUIRED_PERMISSIONS)) {
                        wifiSensor = localWifiSensor = new WifiSensor(context);
                    } else {
                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_WIFI);
                    }
                }
            }
        }
        return wifiSensor;
    }

    private WifiSensor(Context context) {
        super(context);
        wifiManager = WifiDevice.getInstance(context);
        MatchingSkills skills = new MatchingSkills();
        skills.addEvent("android.net.wifi.SCAN_RESULTS");
        skills.addEntity(WifiEvents.EVENT_SCAN_STATE);
//        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION
        CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(skills);
        wifiReceiver = new CommonEventSubscriber(subscribeInfo) {
            @Override
            public void onReceiveEvent(CommonEventData commonEventData) {
                List<WifiScanInfo> wifiList = wifiManager.getScanInfoList();
                for (WifiScanInfo result : wifiList) {
                    int level = wifiManager.getSignalLevel(result.getRssi(), result.getBand());
                    WifiScanResult wifiScanResult = new WifiScanResult(result.getSsid(), result.getBssid(), result.getCapabilities(), level, result.getFrequency());
                    wifiScanResults.add(wifiScanResult);
                }

                cyclesRemaining -= 1;
                if ((cyclesRemaining > 0) && (wifiManager.isWifiActive())) {
                    wifiManager.scan();
                } else {
                    notifySenseCyclesComplete();
                }

            }

        };
    }

    protected String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_WIFI;
    }

    protected WifiData getMostRecentRawData() {
        return wifiData;
    }

    protected void processSensorData() {
        WifiProcessor processor = (WifiProcessor) getProcessor();
        wifiData = processor.process(pullSenseStartTimestamp, wifiScanResults, sensorConfig.clone());
    }

    protected boolean startSensing() {
        try {
            wifiScanResults = null;
            if (wifiManager.isWifiActive()) {
                wifiScanResults = new ArrayList<WifiScanResult>();
                cyclesRemaining = (Integer) sensorConfig.getParameter(PullSensorConfig.NUMBER_OF_SENSE_CYCLES);

                CommonEventManager.subscribeCommonEvent(wifiReceiver);
                wifiManager.scan();
                return true;
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return false;
    }

    // Called when a scan is finished
    protected void stopSensing() {
        try {
            CommonEventManager.unsubscribeCommonEvent(wifiReceiver);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
