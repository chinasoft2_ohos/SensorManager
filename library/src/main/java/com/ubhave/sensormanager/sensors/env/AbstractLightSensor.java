/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ubhave.sensormanager.sensors.env;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.sensors.push.AbstractPushSensor;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.sensor.agent.CategoryLightAgent;
import ohos.sensor.agent.SensorAgent;
import ohos.sensor.bean.CategoryLight;
import ohos.sensor.bean.SensorBase;
import ohos.sensor.data.CategoryLightData;
import ohos.sensor.listener.ICategoryEnvironmentDataCallback;
import ohos.sensor.listener.ICategoryLightDataCallback;

/**
 * AbstractLightSensor
 *
 * @author: zhu_zhonglin
 * @since: 2021/4/25
 */
public abstract class AbstractLightSensor extends AbstractPushSensor {
    protected SensorAgent sensorManager;
    private final SensorBase lightSensor;
    private final ICategoryLightDataCallback sensorEventListener;
    public AbstractLightSensor(Context context) throws ESException {
        super(context);
        sensorManager = new CategoryLightAgent();

        sensorEventListener = getEventListener();
        lightSensor = getSensor();
        if (lightSensor == null) {
            throw new ESException(ESException.SENSOR_UNAVAILABLE, getLogTag() + " is null (missing from device).");
        }
    }

    protected final ICategoryLightDataCallback getEventListener(){
        return new ICategoryLightDataCallback() {
            @Override
            public void onSensorDataModified(CategoryLightData event) {
                try {
                    SensorData data = processEvent(event);
                    onDataSensed(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAccuracyDataModified(CategoryLight categoryLight, int i) {

            }

            @Override
            public void onCommandCompleted(CategoryLight categoryLight) {

            }
        };
    };
    protected abstract SensorData processEvent(final ohos.sensor.data.SensorData event);

    protected abstract SensorBase getSensor();
    @Override
    protected final boolean startSensing() {
        SensorBase sensor = getSensor();
        if (sensor != null) {
            return sensorManager.setSensorDataCallback(sensorEventListener, getSensor(), 3);
        } else {
            return false;
        }

    }

    @Override
    protected final void stopSensing() {
        sensorManager.releaseSensorDataCallback(sensorEventListener);
    }

    @Override
    protected void onBroadcastReceived(Context context, Intent intent) {
        // Unused
    }

    @Override
    protected MatchingSkills getIntentFilters() {
        // Unused
        return null;
    }
}
