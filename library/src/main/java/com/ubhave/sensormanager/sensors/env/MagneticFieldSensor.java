/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.env;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.process.env.MagneticFieldProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.app.Context;
import ohos.sensor.bean.CategoryEnvironment;
import ohos.sensor.bean.SensorBase;

/**
 * 磁场传感器
 */
public class MagneticFieldSensor extends AbstractEnvironmentSensor {
    private static final String TAG = "MagneticFieldSensor";
    private volatile static MagneticFieldSensor magneticFieldSensor;
    protected static final Object lock = new Object();

    public static MagneticFieldSensor getSensor(Context context) throws ESException {
        MagneticFieldSensor localMagneticFieldSensor = magneticFieldSensor;
        if (localMagneticFieldSensor == null) {
            synchronized (lock) {
                localMagneticFieldSensor = magneticFieldSensor;
                if (localMagneticFieldSensor == null) {
                    magneticFieldSensor = localMagneticFieldSensor = new MagneticFieldSensor(context);
                }
            }
        }
        return magneticFieldSensor;
    }

    private MagneticFieldSensor(final Context context) throws ESException {
        super(context);
//        CategoryEnvironment.SENSOR_TYPE_MAGNETIC_FIELD
    }

    @Override
    protected SensorData processEvent(ohos.sensor.data.SensorData event) {
        float[] values = event.values;
        MagneticFieldProcessor processor = (MagneticFieldProcessor) getProcessor();
        return processor.process(values, System.currentTimeMillis(), sensorConfig.clone());
    }

    @Override
    protected SensorBase getSensor() {
        return sensorManager.getSingleSensor(CategoryEnvironment.SENSOR_TYPE_MAGNETIC_FIELD);
    }

    protected String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_MAGNETIC_FIELD;
    }

}
