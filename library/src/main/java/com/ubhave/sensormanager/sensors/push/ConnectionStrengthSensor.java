/* **************************************************
 Copyright (c) 2014, Idiap
Hugues Salamin, hugues.salamin@idiap.ch

This file was developed to add connection strength sensor to the SensorManager library
from https://github.com/nlathia/SensorManager.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.push;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.push.ConnectionStrengthData;
import com.ubhave.sensormanager.process.push.ConnectionStrengthProcessor;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SignalInformation;

import java.util.List;

public class ConnectionStrengthSensor extends AbstractPushSensor {
    private static final String TAG = "ConnectionStrengthSensor";
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            "ohos.permission.GET_TELEPHONY_STATE"
    };

    private volatile static ConnectionStrengthSensor connectionSensor;
    private int primarySlotId = 0;//主卡槽

    public static ConnectionStrengthSensor getSensor(final Context context) throws ESException {
        ConnectionStrengthSensor result = connectionSensor;
        if (result == null) {
            synchronized (ConnectionStrengthSensor.class) {
                result = connectionSensor;
                if (result == null) {
//                    if (allPermissionsGranted(context, REQUIRED_PERMISSIONS)) {
                    connectionSensor = result = new ConnectionStrengthSensor(context);
//                    } else {
//                        permissionAsk(context, REQUIRED_PERMISSIONS, 0x123);
////                        throw new ESException(ESException.PERMISSION_DENIED, SensorUtils.SENSOR_NAME_CONNECTION_STRENGTH);
//                    }
                }
            }
        }
        return result;
    }

    private ConnectionStrengthSensor(Context context) {
        super(context);

    }

    protected MatchingSkills getIntentFilters() {
        return null;
    }

    @Override
    protected void onBroadcastReceived(Context context, Intent intent) {

    }

    protected String getLogTag() {
        return TAG;
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_CONNECTION_STRENGTH;
    }

    protected boolean startSensing() {
        RadioInfoManager infoManager = RadioInfoManager.getInstance(applicationContext);
        primarySlotId = infoManager.getPrimarySlotId();
        List<SignalInformation> signalInfoList = infoManager.getSignalInfoList(primarySlotId);
        SignalInformation signalStrength = signalInfoList.get(0);
//        int type = signalStrength.getNetworkType();
        int signalLevel = signalStrength.getSignalLevel();

        //不在UI线程
        ConnectionStrengthProcessor processor = (ConnectionStrengthProcessor) getProcessor();
        ConnectionStrengthData strengthData = processor.process(System.currentTimeMillis(), sensorConfig.clone(), signalLevel);
        onDataSensed(strengthData);
        return true;
    }

    protected void stopSensing() {
        // nothing to do
    }

}
