/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.sensors.env;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.sensors.push.AbstractPushSensor;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.event.commonevent.MatchingSkills;
import ohos.sensor.agent.CategoryEnvironmentAgent;
import ohos.sensor.agent.SensorAgent;
import ohos.sensor.bean.CategoryEnvironment;
import ohos.sensor.bean.SensorBase;
import ohos.sensor.data.CategoryEnvironmentData;
import ohos.sensor.listener.ICategoryEnvironmentDataCallback;
import ohos.sensor.listener.ISensorDataCallback;

/**
     传感器的定义
     #define SENSOR_TYPE_ACCELEROMETER       1  //加速度传感器
     #define SENSOR_TYPE_MAGNETIC_FIELD       2  //磁力传感器
     #define SENSOR_TYPE_ORIENTATION           3  //方向
     #define SENSOR_TYPE_GYROSCOPE            4  //陀螺仪
     #define SENSOR_TYPE_LIGHT                  5  //环境光照传感器
     #define SENSOR_TYPE_PRESSURE              6  //压力传感器
     #define SENSOR_TYPE_TEMPERATURE          7  //温度传感器
     #define SENSOR_TYPE_PROXIMITY             8   //距离传感器
     #define SENSOR_TYPE_GRAVITY             9
     #define SENSOR_TYPE_LINEAR_ACCELERATION 10   //线性加速度
     #define SENSOR_TYPE_ROTATION_VECTOR     11
     #define SENSOR_TYPE_RELATIVE_HUMIDITY   12    //湿度传感器
     #define SENSOR_TYPE_AMBIENT_TEMPERATURE 13
 */
public abstract class AbstractEnvironmentSensor extends AbstractPushSensor {
    protected SensorAgent sensorManager;

    private final SensorBase environmentSensor;
    private final ICategoryEnvironmentDataCallback sensorEventListener;

    protected AbstractEnvironmentSensor(final Context context) throws ESException {
        super(context);
        sensorManager = new CategoryEnvironmentAgent();
        sensorEventListener = getEventListener();
        environmentSensor = getSensor();
        if (environmentSensor == null) {
            throw new ESException(ESException.SENSOR_UNAVAILABLE, getLogTag() + " is null (missing from device).");
        }
    }

    protected final ICategoryEnvironmentDataCallback getEventListener() {
        return new ICategoryEnvironmentDataCallback() {
            @Override
            public void onSensorDataModified(CategoryEnvironmentData event) {
                try {
                    SensorData data = processEvent(event);
                    onDataSensed(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onAccuracyDataModified(CategoryEnvironment categoryEnvironment, int i) {

            }

            @Override
            public void onCommandCompleted(CategoryEnvironment categoryEnvironment) {

            }
        };
    }

    protected abstract SensorData processEvent(final ohos.sensor.data.SensorData event);

    protected abstract SensorBase getSensor();

    @Override
    protected final boolean startSensing() {
        SensorBase sensor = getSensor();
        if (sensor != null) {
            return sensorManager.setSensorDataCallback(sensorEventListener, getSensor(), 3);
        } else {
            return false;
        }

    }

    @Override
    protected final void stopSensing() {
        sensorManager.releaseSensorDataCallback(sensorEventListener);
    }

    @Override
    protected void onBroadcastReceived(Context context, Intent intent) {
        // Unused
    }

    @Override
    protected MatchingSkills getIntentFilters() {
        // Unused
        return null;
    }
}
