package com.ubhave.sensormanager.process.push;


import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.push.ConnectionStateData;
import com.ubhave.sensormanager.process.AbstractProcessor;
import ohos.app.Context;
import ohos.net.NetHandle;
import ohos.wifi.WifiLinkedInfo;

public class ConnectionStateProcessor extends AbstractProcessor {
    public ConnectionStateProcessor(final Context c, boolean rw, boolean sp) {
        super(c, rw, sp);
    }

    public ConnectionStateData process(long recvTime, SensorConfig config, NetHandle activeNetwork, WifiLinkedInfo wifiInfo) {
        ConnectionStateData data = new ConnectionStateData(recvTime, config);
        if (setRawData) {
            data.setNetworkType(activeNetwork);
            data.setWifiDetails(wifiInfo);
        }
        return data;
    }

}
