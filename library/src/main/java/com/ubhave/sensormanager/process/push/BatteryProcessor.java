package com.ubhave.sensormanager.process.push;

import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.push.BatteryData;
import com.ubhave.sensormanager.process.AbstractProcessor;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.app.Context;
import ohos.batterymanager.BatteryInfo;

public class BatteryProcessor extends AbstractProcessor {
    public BatteryProcessor(Context c, boolean rw, boolean sp) {
        super(c, rw, sp);
    }

    public BatteryData process(long recvTime, SensorConfig config, Intent dataIntent) {
        BatteryData data = new BatteryData(recvTime, config);
        if (setRawData) {
            IntentParams params = dataIntent.getParams();

            //当前电量
            int level = (int) params.getParam(BatteryInfo.OHOS_BATTERY_CAPACITY);
            data.setBatteryLevel(level);

            //充电类型
            int chargeType = (int) params.getParam(BatteryInfo.OHOS_CHARGE_TYPE);
            for (BatteryInfo.BatteryPluggedType status : BatteryInfo.BatteryPluggedType.values()) {
                if (chargeType == status.ordinal()) {
                    data.setPlugType(status);
                    break;
                }
            }

//            int scale = dataIntent.getIntParam(BatteryManager.EXTRA_SCALE, -1);
//            data.setScale(scale);

            //电池温度
            int temp = (int) params.getParam(BatteryInfo.OHOS_BATTERY_TEMPERATURE);
            data.setTemperature(temp);

            //电池电压
            int voltage = (int) params.getParam(BatteryInfo.OHOS_BATTERY_VOLTAGE);
            data.setVoltage(voltage);

            //电池充电循环次数
            int chargeCounter = (int) params.getParam(BatteryInfo.OHOS_CHARGE_COUNTER);
            data.setChargeCounter(chargeCounter);

            //电池电量状态
            int chageState = (int) params.getParam(BatteryInfo.OHOS_CHARGE_STATE);
            for (BatteryInfo.BatteryChargeState status : BatteryInfo.BatteryChargeState.values()) {
                if (chageState == status.ordinal()) {
                    data.setStatus(status);
                    break;
                }
            }

            //电池温度健康状态
            int health = (int) params.getParam(BatteryInfo.OHOS_TEMPERATURE_STATE);
            for (BatteryInfo.BatteryHealthState state : BatteryInfo.BatteryHealthState.values()) {
                if (health == state.ordinal()) {
                    data.setHealth(state);
                    break;
                }
            }
        }
        return data;
    }

}
