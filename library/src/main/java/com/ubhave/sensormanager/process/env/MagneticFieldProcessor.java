package com.ubhave.sensormanager.process.env;

import java.util.ArrayList;

import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.env.MagneticFieldData;
import com.ubhave.sensormanager.process.AbstractProcessor;
import ohos.app.Context;

public class MagneticFieldProcessor extends AbstractProcessor {
    public MagneticFieldProcessor(final Context c, boolean rw, boolean sp) {
        super(c, rw, sp);
    }

    protected MagneticFieldData getInstance(long pullSenseStartTimestamp, SensorConfig sensorConfig) {
        return new MagneticFieldData(pullSenseStartTimestamp, sensorConfig);
    }

    public MagneticFieldData process(float[] sensorReadings, long sensorReadingTimestamps, SensorConfig sensorConfig) {
        // Future: feature extraction
        MagneticFieldData data = getInstance(sensorReadingTimestamps, sensorConfig);
        data.setPrevSensorData(data);
        data.setxAxis(sensorReadings[0]);
        data.setyAxis(sensorReadings[1]);
        data.setzAxis(sensorReadings[2]);
        return data;
    }
}
