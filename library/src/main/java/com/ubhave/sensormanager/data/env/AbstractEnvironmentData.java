

package com.ubhave.sensormanager.data.env;

import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.SensorData;

public abstract class AbstractEnvironmentData extends SensorData {
    private float value;

    public AbstractEnvironmentData(final long recvTimestamp, final SensorConfig sensorConfig) {
        super(recvTimestamp, sensorConfig);
    }

    public void setValue(float v) {
        this.value = v;
    }

    public float getValue() {
        return value;
    }
}
