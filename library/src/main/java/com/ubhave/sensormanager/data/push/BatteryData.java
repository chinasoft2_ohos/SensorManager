/* **************************************************
 Copyright (c) 2012, University of Cambridge
 Neal Lathia, neal.lathia@cl.cam.ac.uk
 Kiran Rachuri, kiran.rachuri@cl.cam.ac.uk

This library was developed as part of the EPSRC Ubhave (Ubiquitous and
Social Computing for Positive Behaviour Change) Project. For more
information, please visit http://www.emotionsense.org

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ************************************************** */

package com.ubhave.sensormanager.data.push;

import com.ubhave.sensormanager.config.SensorConfig;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.sensors.SensorUtils;
import ohos.batterymanager.BatteryInfo;
import ohos.utils.Parcel;

public class BatteryData extends SensorData {
    private int level;
    private int scale;
    private int temperature;
    private int voltage;
    private int chargeCounter;
    private BatteryInfo.BatteryPluggedType plug_type;
    private BatteryInfo.BatteryChargeState status;
    private BatteryInfo.BatteryHealthState health;

    public BatteryData(long timestamp, SensorConfig config) {
        super(timestamp, config);
    }

    public void setPlugType(BatteryInfo.BatteryPluggedType type) {
        plug_type = type;
    }

    public String getPlugType() {
        return plug_type.name();
    }

    public boolean plugSourceUSB() {
        return plug_type == BatteryInfo.BatteryPluggedType.USB;
    }

    public boolean plugSourceAC() {
        return plug_type == BatteryInfo.BatteryPluggedType.AC;
    }

    public void setBatteryLevel(int l) {
        level = l;
    }

    public int getBatteryLevel() {
        return level;
    }

    public int getChargeCounter() {
        return chargeCounter;
    }

    public void setChargeCounter(int chargeCounter) {
        this.chargeCounter = chargeCounter;
    }


    public void setScale(int s) {
        scale = s;
    }

    public int getScale() {
        return scale;
    }

    public void setTemperature(int t) {
        temperature = t;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setVoltage(int v) {
        voltage = v;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setStatus(BatteryInfo.BatteryChargeState s) {
        status = s;
    }

    public BatteryInfo.BatteryChargeState getStatus() {
        return status;
    }

    public void setHealth(BatteryInfo.BatteryHealthState h) {
        health = h;
    }

    public BatteryInfo.BatteryHealthState getHealth() {
        return health;
    }

    public boolean isCharging() {
        if (status == BatteryInfo.BatteryChargeState.ENABLE) {
            return true;
        } else {
            return false;
        }
    }

    public int getSensorType() {
        return SensorUtils.SENSOR_TYPE_BATTERY;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
